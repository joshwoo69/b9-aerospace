PART
{
    // --- general parameters ---
    name = B9_Engine_Jet_Turbofan_F119
    module = Part
    author = bac9

    // --- asset parameters ---
    mesh = model.mu
    scale = 1.0
    rescaleFactor = 1
	CoMOffset = 0, 2, 0

    // --- node definitions ---
    node_stack_top = 0.0, 0.5, 0.0, 0.0, 1.0, 0.0

    // --- editor parameters ---
    TechRequired = aerospaceTech
    entryCost = 15000
    cost = 2500
    category = Engine
    subcategory = 0
    title = F119 Turbofan Engine
    manufacturer = Tetragon Projects
    description = State-of-art  afterburning low-bypass turbofan engine capable of sustained supercruise speeds. Fitted with latest in thrust vectoring technology, this engine provides extreme pitch authority, and, if multiple engines are present, outstanding roll authority. Obvious choice whenever superior maneuverability is desired. Max Design Speed: Mach 2.5.

    // attachment rules: stack, srfAttach, allowStack, allowSrfAttach, allowCollision
    attachRules = 1,0,1,0,0

    // --- standard part parameters ---
    mass = 1.8
    dragModelType = default
    maximum_drag = 0.2
    minimum_drag = 0.2
    angularDrag = 2
    crashTolerance = 7
    breakingForce = 75
    breakingTorque = 75
    maxTemp = 2000 // = 3600
	skinInternalConductionMult = 4.0
	emissiveConstant = 0.8
	bulkheadProfiles = size1

    EFFECTS
    {
        power
        {
            MODEL_MULTI_PARTICLE_PERSIST
            {
                name = b9_jet_f119_flame
                modelName = B9_Aerospace/FX/HR/flamejet3
                transformName = thrust_transform
                localPosition = 0, 0, -0.5
                fixedScale = 0.67
                emission = 0.66 0
                emission = 0.67 2.42 1.35 1.35
                emission = 1.0 2.5 0 0
                speed = 0.66 3.75
                speed = 1.0 4.575
                energy = 0.66 0.5
                energy = 1 1
                // under/over expansion
                logGrow
                {
                    density = 1 -0.9
                    density = 0.4 1
                    density = 0.05 5
                    density = 0.005 40
                }
                linGrow
                {
                    density = 0.05 0 0 0
                    density = 0.005 3
                    density = 0 30
                }
                size
                {
                    power = 0.66 1.8
                    power = 0.7 1.85
                    power = 1 1.9
                    density = 0.4 1
                    density = 0.05 0.87
                    density = 0.03 0.55
                    density = 0.02 0.45
                    density = 0.005 0.4
                    density = 0 0.25 0 0
                }
            }

            AUDIO
            {
                name = b9_jet_f119_sound_afterburner
                channel = Ship
                clip = sound_rocket_hard
                volume = 0.66 0
                volume = 0.67 1 3 3
                volume = 1 1.33 0 0
                pitch = 0.66 1
                pitch = 1 0.66
                loop = true
            }

            AUDIO
            {
                name = b9_jet_f119_sound_jet_deep
                channel = Ship
                clip = sound_jet_deep
                volume = 0   0    0     25
                volume = 0.1 1.12 0.145 0.145
                volume = 1.0 1.25 0.145 0
                pitch = 0 0.3
                pitch = 1 1
                loop = true
            }
        }
        running
        {
            MODEL_MULTI_PARTICLE_PERSIST
            {
                name = b9_jet_f119_smoke
                modelName = B9_Aerospace/FX/HR/smokejet
                transformName = thrust_transform
                speed = 0.0 0.8
                speed = 1.0 1
                size = 0.75

                emission
                {
                    power = 0.07 0 0 0
                    power = 0.2 0.4 2.91 2.91
                    power = 1.0 1.1 0 0
                    density = 0.001 0 0 0
                    density = 0.002 1 0 0
                }

                energy
                {
                    power = 0 0.2
                    power = 1 3.2
                    externaltemp = -10 1
                    externaltemp = 1 0
                    mach = 0 1
                    mach = 10 10
                }

                offset
                {
                    mach = 0 1
                    mach = 6 10
                }

                logGrow
                {
                    density = 0 2
                    density = 1 0.9
                    density = 10 0
                }
            }

            AUDIO
            {
                channel = Ship
                clip = sound_jet_low
                volume = 0   0    0    25
                volume = 0.1 1.12 0.22 0.22
                volume = 1   1.32 0.22 0
                pitch = 0 0.3
                pitch = 1 1
                loop = true
            }
        }
        engage
        {
            AUDIO
            {
                channel = Ship
                clip = sound_vent_medium
                volume = 1.0
                pitch = 2.0
                loop = false
            }
        }
        disengage
        {
            AUDIO
            {
                channel = Ship
                clip = sound_vent_soft
                volume = 1.0
                pitch = 2.0
                loop = false
            }
        }
        flameout
        {
            PREFAB_PARTICLE
            {
                prefabName = fx_exhaustSparks_flameout_2
                transformName = thrust_transform
                oneShot = true
                localOffset = 0, 0, 2.2
            }

            AUDIO
            {
                channel = Ship
                clip = sound_explosion_low
                volume = 1.0
                pitch = 2.0
                loop = false
            }
        }
    }
	
	MODULE
    {
        name = ModuleEnginesFX
        engineID = Cruise
        thrustVectorTransformName = thrust_transform
        exhaustDamage = False
        ignitionThreshold = 0.1
        minThrust = 0
        maxThrust = 160
        heatProduction = 120 // Modified in global config
        useEngineResponseTime = True
        engineAccelerationSpeed = 0.25
        engineDecelerationSpeed = 0.45
        useVelocityCurve = False
        EngineType = Turbine
        PROPELLANT
        {
            name = LiquidFuel
			resourceFlowMode = STAGE_PRIORITY_FLOW
            ratio = 1
            DrawGauge = True
        }
        PROPELLANT
        {
            name = IntakeAir
            ignoreForIsp = True
			ratio = 30
        }
        atmChangeFlow = True
		useVelCurve = True
		useAtmCurve = False
		machLimit = 1.75
		machHeatMult = 200.0
		atmosphereCurve
        {
            key = 0 5440 0 0
        }
        velCurve
		{
			key = 0.0 1.0 -0.207353362145 -0.207353362145
			key = 0.09 0.994292641105 -0.104834651632 -0.104834651632
			key = 2.99 9.68023869113 7.1579426814 7.1579426814
			key = 3.58 12.1274147138 0.447510590625 0.447510590625
			key = 4.43 4.63175651834 -16.4125126004 -16.4125126004
			key = 4.8 0.0 -6.63827797999 -6.63827797999
		}
    }

    MODULE
    {
        name = FSanimateThrottle
        animationName = jet_engine_f119_throttle
        responseSpeed = 0.0025
        animationLayer = 2
    }

    MODULE
    {
        name = ModuleAnimateHeat
        ThermalAnim = jet_engine_f119_heat
    }

    MODULE
    {
        name = ModuleAlternator
        RESOURCE
        {
            name = ElectricCharge
            rate = 5.0
        }
    }

    MODULE
    {
        name = KM_Gimbal_3
        gimbalTransformName = thrust_transform
        pitchGimbalRange = 20
        yawGimbalRange = 0
        enableRoll = true
        enableSmoothGimbal = true
        responseSpeed = 60
    }

    RESOURCE
    {
        name = ElectricCharge
        amount = 0
        maxAmount = 0
        isTweakable = false
        hideFlow = true
    }

    MODULE
    {
        name = FXModuleConstrainPosition
        matchRotation = true
        matchPosition = false
        CONSTRAINFX
        {
            targetName = lookatfx_target1
            moversName = plate1_holder
        }

        CONSTRAINFX
        {
            targetName = lookatfx_target2
            moversName = plate2_holder
        }
    }

    MODULE
    {
        name = ModuleTestSubject
        environments = 71
        useStaging = True
        useEvent = True
    }
	
	MODULE
	{
		name = ModuleSurfaceFX
		thrustProviderModuleIndex = 0
		fxMax = 0.5
		maxDistance = 20
		falloff = 2
		thrustTransformName = thrust_transform
	}
}

# Transonic buff for stock aero
@PART[B9_Engine_Jet_Turbofan_F119]:NEEDS[!FerramAerospaceResearch]
{
	@MODULE[ModuleEngines*]
	{
		!velCurve {}
		velCurve
		{
			key = 0.0 1.0 -0.462259983287 -0.462259983287
			key = 0.09 0.994292641105 -0.444411625831 -0.444411625831
			key = 0.77 2.08782608926 3.61950804648 3.61950804648
			key = 0.8 2.18468827506 3.27408676688 3.27408676688
			key = 1.0 2.76848864513 2.45210131582 2.45210131582
			key = 1.64 4.66330069794 3.3228360073 3.3228360073
			key = 2.18 6.35514614877 3.20823011399 3.20823011399
			key = 3.0 9.74906124187 6.94773342963 6.94773342963
			key = 3.58 12.1274147138 0.403875621023 0.403875621023
			key = 4.43 4.63175651834 -16.4440990432 -16.4440990432
			key = 4.8 0.0 -6.66196788876 -6.66196788876
		}
		@useAtmCurve = True
		atmCurve
		{
			key = 0 0 0 0
			key = 0.1 0.1 1.276916 1.276916
			key = 0.297 0.35 1.304143 1.304143
			key = 0.538 0.59 0.8485174 0.8485174
			key = 1 1 0.8554117 0
		}
	}
}